
# "Back-End" Car Management Dashboard (Chapter 5 - Main Challenge)

A simple car management dashboard where you can do basic CRUD operation with it.

## Description

This is the final project for me to pass this Chapter 5 and continue to the next chapter, in this chapter we were asked to create a simple car management information system where we can perform basic CRUD operations on the car data there. This project has used a database with PostgreSQL as the DBMS. Using Node.js to create HTTP Server and Express comes to play as the framework, renders HTML elements using View Engine EJS, and uses the Sequelize module as the ORM.

## Tech Stack

* Client :
    * SB Admin 2 (Admin Template)
    * EJS 3.1.7 (View Engine)
    
* Server :
    * Node.js 16.14 (Runtime Environment)
    * Express 4.17.3 (HTTP Server)
    * EJS 3.1.7 (View Engine)
    * Sequelize 6.19.0 (ORM)
    * PostgreSQL 14.2 (DBMS)
    * Axios 0.26.1 (HTTP Request)
    * Multer 1.4.4 (Uploading Files)

## Prerequisite

These are things that you need to install on your machine before proceed to installation
* [Node.js](https://nodejs.org/)
* [NPM (Package Manager)](https://www.npmjs.com/)
* [PostgreSQL](https://www.postgresql.org/) 
* [Postman](https://www.postman.com/)


## Installation

Clone this repository

```bash
cd desired_directory/
git clone https://gitlab.com/frizaadityafrinison/car-management-dashboard
cd car-management-dashboard
```

Download all the package and it's dependencies
```bash
npm install 
```

Install the Sequelize
```bash
npm install sequelize-cli -g
```

Change the config.js
```bash
cd config/config.json
cat config.json
```

Create The Database
```bash
sequelize db:create
```

Create The Model
```bash
sequelize model:generate --name Car --attributes nama:text,harga:integer,ukuran:string,foto:text
```

Run The Migration
```bash
sequelize db:migrate
``` 

## Running Back-End

To run the back-end, run the following command

```bash
  npm run start
```


## API Reference
This server running on port 8080, test the API using Postman
#### Get all items

```http
  GET /
```

![Read](/uploads/5abbcbec64309f31359b8ad020298ffb/Read.png)

#### Get item by ID

```http
  GET /${id}
```

![Read_By_ID](/uploads/8c86753f31ca1dcd63dd03cadacaf14f/Read_By_ID.png)

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `integer` | **Required**. Id of item to get |


#### Create item

```http
  POST /
```

![Create](/uploads/d45be1ded71655b888999d728e6e3535/Create.png)

#### Delete item

```http
  DELETE /${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `integer` | **Required**. Id of item to delete |

![Delete](/uploads/c86351c77a27f6d73ca1e022524a017c/Delete.png)

#### Update item

```http
  PUT /${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `integer` | **Required**. Id of item to update |

![Update](/uploads/169c99bc76785e990c82727d32796aec/Update.png)

## Entity Relationship Diagram

![ERD](/uploads/b61f01b403de637f1df2975790285c36/ERD.png)
