const multer = require('multer');
const { Car } = require('../models');

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, '../img')
    },
    filename: function(req, file, cb){
        console.log(file);
        cb(null, file.originalname)
    }
})

module.exports = {
    list: async (req, res) => { // ---------------------------------------------------------READ ALL CARS
        try {
            const data = await Car.findAll();

            return res.status(400).json({
                success: true,
                error: false,
                data: data,
                message: "Data SUCCEED To POPULATED!"
            });
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error
            });
        }
    },
    listById: async (req, res) => { // ---------------------------------------------------------READ/:id a CAR
        try { 
            Car.findAll({
                where:{
                    id: req.params.id
                }
            }).then(cars=>{
                res.json(cars)
            })
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                message: error
            });
        }
    },
    destroy: async (req, res) => { // ---------------------------------------------------------DELETE A CAR
        try {
            const { id } = req.params;            
            Car.destroy({
                where: { 
                    id: id 
                }
            }).then(() =>{
                res.json({
                    success: true,
                    error: false,
                    message: "Data SUCCEED To DELETE!"
                })
            })
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                message: "Data Failed To DELETE!"
            });
        }
    },
    create: async (req, res) => { // ---------------------------------------------------------CREATE A CAR
        try {
            let upload = multer({ storage:storage }).single('foto');

            upload(req, res, function(err){
                Car.create({
                    nama: req.body.nama,
                    harga: req.body.harga,
                    ukuran: req.body.ukuran,
                    foto: req.file ? req.file.originalname : '',
                })
                res.json({
                    success: true,
                    error: false,
                    message: "Data SUCCEED To Create"
                })
            })
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: "Data Failed To Create!"
            });
        }
    },
    update: async (req, res) => { // ---------------------------------------------------------UPDATE A CAR
        try {
            let upload = multer({ storage:storage }).single('foto');

            const{ id } = req.params;

            upload(req, res, function(err){
                Car.update({
                    nama: req.body.nama,
                    harga: req.body.harga,
                    ukuran: req.body.ukuran,
                    foto: req.file ? req.file.originalname : '',
                }, {
                    where: {id:id}
                }).then(() => {
                    res.json({
                        success: true,
                        error: false,
                        message: "Data SUCCEED To UPDATE!"
                    })
                })
            })
        } catch (error) {
            return res.status(400).json({
                success: false,
                error: true,
                message: "Data Failed To UPDATE!"
            });
        }
    }
}
