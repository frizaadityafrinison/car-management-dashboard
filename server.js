const express = require('express');
const app = express();
const port = 8080;
const { create, destroy, list, update, listById } = require('./controller/car');

app.use(express.static(`public`));
app.use(express.urlencoded({extended:true}));
app.set(`view engine`, 'ejs');
app.use(express.json());

// LIST ALL CARS
app.get('/', list);

// DELETE A CAR BY ID
app.delete('/:id', destroy);

// CREATE A CAR
app.post('/', create);

// UPDATE A CAR BY ID
app.get('/:id', listById);
app.put('/:id', update);

app.listen(port, () => {
    console.log(`Listening On Port ${port}`);
})